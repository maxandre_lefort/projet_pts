// Processing functions
#include "processing.h"
#include "fixFFT/fixFFT.h"
#include <stdint.h>
#include <unistd.h>

void benchmark(int16_t* sig_out,int16_t* sig,int64_t size){
   //TODO Iteration 4: You will use this function, but nothing has to be modified in the function
    int mc_runs = 1;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    for (int c = 0 ; c < mc_runs ; c++){
        processing(sig_out,sig,size);
    }
    timer = toc(timer);
    printf("output\n");
    print_toc(timer);
}


int processing(int16_t* sig_out,int16_t* sig, int64_t size)
{
   //TODO Iteration 2: This should fill the function to synthetize sig_out, a new signal (independant from sig)
	sine_wave(sig_out,512,size);
	//TODO Iteration 3: This should fill the function to synthetize a sig_out, new signal based from sig
   //TODO Iteration 4: This should update this function to synthetize a signal sig_out, with part of sig
    return 0;
}



/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB 
 * => Ensure double precision multiplier and shift to take MSB 
 */
inline int16_t mul_int16_int16(int16_t x,int16_t y)
{
    return  (int32_t)(x *  y) >> 16;
}

int16_t fpsin_restricted(int16_t x_bar)
{
	//x_bar Q(16,1,14)

	int16_t a_fixCode = 25718; // Q(16,1,14)
	int16_t b_fixCode = -10477; // Q(16,1,14)
	int16_t c_fixCode = 1142; // Q(16,1,14)

	int16_t x_2 = mul_int16_int16(x_bar, x_bar); // Q(16,3,12)
	int16_t x_3 = mul_int16_int16(x_bar, x_2); // Q(16,5,10)
	x_3 = x_3 << 4; // Q(16,1,14)

	int16_t x_4 = mul_int16_int16(x_3, x_bar); // Q(16,3,12)
	int16_t x_5 = mul_int16_int16(x_4, x_bar); // Q(16,5,10)
	x_5 = x_5 << 4; // Q(16,1,14)

	int16_t m0 = mul_int16_int16(a_fixCode,x_bar); // Q(16,3,12)
	int16_t m1 = mul_int16_int16(b_fixCode,x_3); // Q(16,3,12)
	int16_t m2 = mul_int16_int16(c_fixCode,x_5); // Q(16,3,12)

	int16_t a1 = m1+m2; // Q(16,3,12)
	int16_t a0 = a1+m0; // Q(16,3,12)

	int16_t out = a0 << 3; //Q(16.0.15)

	return out;
}


int16_t fpsin(int16_t x)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain ! 
	int16_t moins_un = -16384; //Q(16,1,14)
	int16_t un = 16384; //Q(16,1,14)
	int16_t deux = 32768; //Q(16,1,14)
	int16_t moins_deux = -32768; //Q(16,1,14)

	int16_t out = 0;
	int16_t sin_normalized;
	int16_t sin_bar;


	//selection of the time domain
	if(-un <= x && x <= un)
	{
		sin_normalized = fpsin_restricted(x); //Q(16,0,15)
		out = sin_normalized; //%Q(16,0,15)
	}
	else if(x > un)
	{
		x = x + moins_deux; //Q(16,1,14)
		sin_bar = fpsin_restricted(x); //Q(16,0,15)
		sin_normalized = mul_int16_int16(sin_bar,moins_un);//Q(16,2,13)
		out = sin_normalized << 2; //Q(16,0,15)
	}
	else
	{
		x = x + deux; //Q(16,1,14)
		sin_bar = fpsin_restricted(x); //Q(16,0,15)
		sin_normalized = mul_int16_int16(sin_bar,moins_un);//Q(16,2,13)
		out = sin_normalized << 2; //Q(16,0,15)
	}

	return out;

}



void sine_wave(int16_t* sig_out, int16_t Fc, int64_t N){

	int16_t w_temp = Fc << 1; //Q(16,1,14)
	int16_t quatre = 4 << 13; //Q(16,3,12)
	int16_t w = mul_int16_int16(w_temp,quatre); //Q(16,5,10)
	w = w << 4; //%Q(16,1,14)

	int16_t wtn = 0;
	for (int64_t i = 0; i < N; i++)
	{
		wtn = wtn+w; // Doit etre en Q(16,1,14)
			        //=> periode de 4 par overflow
		sig_out[i] = fpsin(wtn);
	}

}
