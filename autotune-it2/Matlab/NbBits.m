function    mx = NbBits(x)

%   Inputs:
%       x: data dynamic
%
%   Output
%       mx : number of bits required to code x

% --- Return number of bits required to code the integer part
% of entry x
% --- Considering that data can be atmost on 32bits
%disp(x)
% n = 1;
% bitValue = 1;
% while bitValue <= abs(x)
%     n = n+1;
%     bitValue = bitValue*2;
% 
% end
% 
% mx = n-1;%'A completer';
%disp(x)
if max(abs(x(:)')) == 0
    mx = 0;
else
    mx = floor(log2(max(abs(x(:)'))))+1; 
end

end
