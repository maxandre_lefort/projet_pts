a = 12/pi - 9/4;
a_fixCode = round(a*(2^14));

b = -2*a+5/2;
b_fixCode = round(b*(2^14));

c = a-1.5;
c_fixCode = round(c*(2^14));

t = -2:2/100:2;
t3 = t.^3;
t5 = t.^5;
sig_f = a*t+b*t.^3+c*t.^5;
t_fix = round(t*(2^14));
t_fix3 = round(t3*(2^14));
t_fix5 = round(t5*(2^14));
sig = a_fixCode*t_fix+(b_fixCode)*(t_fix.^3) + (c_fixCode)*(t_fix.^5); 
% figure;
% plot(t,sig_f,'+')
% title("Float point sin");
% figure;
% plot(t,sig,'+')
% title("Fix point sin");
% figure;
% sig_func = sin_2(t_fix);
% 
% close all
% clc
% % plot(t_fix,sig_func)
% %title("Fix point sin with the function");
% hold on;
% %plot(t,sin(t))
% legend;

sinuse = sine_wave(1002,30000);

sound(sinuse,8000)

function res = multiplier(a,b)
res = int16(int32(a) .* int32(b)  ./ 2^16);
end

function tmp = add_int16_int16(a,b)
    % Bounds for the given type 
    vMax = lshift(1,15)-1;
    vMin = -lshift(1,15);
    % Pure sum, not typed 
    tmp = a + b ;
    % Check values 
    if tmp > vMax 
        tmp = vMin + (tmp - vMax);
    elseif tmp < vMin 
        tmp = vMax - (-tmp + vMin); 
    end
    tmp = double(tmp);
end

function out = f_fixSine(x_bar) %% Q(16,1,14)
a_fixCode = 25718; % Q(16,1,14)
b_fixCode = -10477; % Q(16,1,14)
c_fixCode = 1142; % Q(16,1,14)

x_2 = multiplier(x_bar, x_bar); % Q(16,3,12)
x_3 = multiplier(x_bar, x_2);% Q(16,5,10)
x_3 = lshift(x_3,4); % Q(16,1,14)

x_4 = multiplier(x_3, x_bar); % Q(16,3,12)
x_5 = multiplier(x_4, x_bar); % Q(16,5,10)
x_5 = lshift(x_5,4); % Q(16,1,14)

m0 = multiplier(a_fixCode,x_bar); % Q(16,3,12)
m1 = multiplier(b_fixCode,x_3); % Q(16,3,12)
m2 = multiplier(c_fixCode,x_5); % Q(16,3,12)

a1 = int16(add_int16_int16(m1,m2)); % Q(16,3,12)
a0 = int16(add_int16_int16(a1,m0)); % Q(16,3,12)

out = lshift(a0, 3); % Q(16,0,15)
 
end

function outV = sin_2(X)

    moins_un = -16384; %Q(16,1,14)
    un = 16384;%Q(16,1,14)
    deux = 32768;%Q(16,1,14)
    moins_deux = -32768;%Q(16,1,14)
    %select the domain 

    outV = [];
    for k = 1 : 1 : length(X)
        x_bar = X(k);
        
    out = 0;
%     disp(x)
    if (-un<=x_bar) & (x_bar <=un)
        
        sin_normalized = f_fixSine(x_bar);%Q(16,0,15)
        out = sin_normalized;%Q(16,0,15)
%         disp("here 1")
    end
    if x_bar > un
%         disp("here 2")
        x_bar = add_int16_int16(x_bar, moins_deux);%Q(16,1,14)
        %x_bar = lshift(x_bar,0);%Q(16,1,14)
        sin_bar = f_fixSine(x_bar);%Q(16,0,15)
        sin_normalized = multiplier(sin_bar, moins_un);%Q(16,2,13)
        out = lshift(sin_normalized,2);%Q(16,0,15)

    end
    if x_bar <= -un
%         disp("here 3") 
        x_bar = add_int16_int16(x_bar,deux);%Q(16,1,14)
        %x_bar2 = lshift(x_bar,0);%Q(16,1,14)
        sin_bar = f_fixSine(x_bar);%Q(16,0,15)
        sin_normalized = -sin_bar;%Q(16,0,15)
        out = lshift(sin_normalized,0);%Q(16,0,15)
    end

    outV(k) = out;
    end
end




function out = sine_wave(Fc,N)
     
     Fe = 8000;
     %Fc Q(16,12,3)
     %Fc/Fe % Q(16,-1,16)

     w_temp = lshift(Fc,1);% Q(16,1,14)
     %4 : Q(16,3,12)
     quatre = lshift(4,12);%Q(16,3,12)
     w = multiplier(w_temp,quatre); %Q(16,5,10)
     w = lshift(w,4);
     %w = round((4*Fc/Fe)*2^14);%Q(16,1,14)
    out = [];
    wtn = 0;
    for i = 1:N
        wtn = add_int16_int16(wtn,w);% Doit etre en Q(16,1,14)
                                        %=> periode de 4 par overflow
        out(i) = sin_2(wtn);
    end


end