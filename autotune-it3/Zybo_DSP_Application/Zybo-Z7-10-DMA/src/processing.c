// Processing functions
#include "processing.h"
#include "fixFFT/fixFFT.h"
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

void benchmark(int16_t* sig_out,int16_t* sig,int64_t size){
   //TODO Iteration 4: You will use this function, but nothing has to be modified in the function
    int mc_runs = 1;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    for (int c = 0 ; c < mc_runs ; c++){
        processing(sig_out,sig,size);
    }
    timer = toc(timer);
    printf("output\n");
    print_toc(timer);
}


int processing(int16_t* sig_out,int16_t* sig, int64_t size)
{
   //TODO Iteration 2: This should fill the function to synthetize sig_out, a new signal (independant from sig)
	//sine_wave(sig_out,512,size);
	//TODO Iteration 3: This should fill the function to synthetize a sig_out, new signal based from sig
	printf("Pich synthesis\n");
	pitch_synthesis(sig,sig_out,8000,size);
   //TODO Iteration 4: This should update this function to synthetize a signal sig_out, with part of sig
    return 0;
}



/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB 
 * => Ensure double precision multiplier and shift to take MSB 
 */
inline int16_t mul_int16_int16(int16_t x,int16_t y)
{
    return  (int32_t)(x *  y) >> 16;
}

int16_t fpsin_restricted(int16_t x_bar)
{
	//x_bar Q(16,1,14)

	int16_t a_fixCode = 25718; // Q(16,1,14)
	int16_t b_fixCode = -10477; // Q(16,1,14)
	int16_t c_fixCode = 1142; // Q(16,1,14)

	int16_t x_2 = mul_int16_int16(x_bar, x_bar); // Q(16,3,12)
	int16_t x_3 = mul_int16_int16(x_bar, x_2); // Q(16,5,10)
	x_3 = x_3 << 4; // Q(16,1,14)

	int16_t x_4 = mul_int16_int16(x_3, x_bar); // Q(16,3,12)
	int16_t x_5 = mul_int16_int16(x_4, x_bar); // Q(16,5,10)
	x_5 = x_5 << 4; // Q(16,1,14)

	int16_t m0 = mul_int16_int16(a_fixCode,x_bar); // Q(16,3,12)
	int16_t m1 = mul_int16_int16(b_fixCode,x_3); // Q(16,3,12)
	int16_t m2 = mul_int16_int16(c_fixCode,x_5); // Q(16,3,12)

	int16_t a1 = m1+m2; // Q(16,3,12)
	int16_t a0 = a1+m0; // Q(16,3,12)

	int16_t out = a0 << 3; //Q(16.0.15)

	return out;
}


int16_t fpsin(int16_t x)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain ! 
	int16_t moins_un = -16384; //Q(16,1,14)
	int16_t un = 16384; //Q(16,1,14)
	int16_t deux = 32768; //Q(16,1,14)
	int16_t moins_deux = -32768; //Q(16,1,14)

	int16_t out = 0;
	int16_t sin_normalized;
	int16_t sin_bar;


	//selection of the time domain
	if(-un <= x && x <= un)
	{
		sin_normalized = fpsin_restricted(x); //Q(16,0,15)
		out = sin_normalized; //%Q(16,0,15)
	}
	else if(x > un)
	{
		x = x + moins_deux; //Q(16,1,14)
		sin_bar = fpsin_restricted(x); //Q(16,0,15)
		sin_normalized = mul_int16_int16(sin_bar,moins_un);//Q(16,2,13)
		out = sin_normalized << 2; //Q(16,0,15)
	}
	else
	{
		x = x + deux; //Q(16,1,14)
		sin_bar = fpsin_restricted(x); //Q(16,0,15)
		sin_normalized = mul_int16_int16(sin_bar,moins_un);//Q(16,2,13)
		out = sin_normalized << 2; //Q(16,0,15)
	}

	return out;

}



void sine_wave(int16_t* sig_out, int16_t Fc, int64_t N){

	int16_t w_temp = Fc << 1; //Q(16,1,14)
	int16_t quatre = 4 << 13; //Q(16,3,12)
	int16_t w = mul_int16_int16(w_temp,quatre); //Q(16,5,10)
	w = w << 4; //%Q(16,1,14)

	int16_t wtn = 0;
	for (int64_t i = 0; i < N; i++)
	{
		wtn = wtn+w; // Doit etre en Q(16,1,14)
			        //=> periode de 4 par overflow
		sig_out[i] = fpsin(wtn);
	}

}


// Iteration 3

int16_t indice_max(int16_t * sig_in, int16_t size)
{
	int16_t max = 0;
	int16_t Imax = 0;
	for(int16_t i = 0; i < size; i++)
	{
		if(sig_in[i] > max)
		{
			Imax = i;
			max = sig_in[i];
		}
	}

	return Imax;
}
int16_t indice_min(int16_t * sig_in,int16_t size){
	int16_t min = 10000;
		int16_t Imin = 0;
		for(int16_t i = 0; i < size; i++)
		{
			if(sig_in[i] < min)
			{
				Imin = i;
				min = sig_in[i];
			}
		}

		return Imin;

}

int16_t absolute(int16_t x)
{
	int16_t output;
	if(x < 0)
	{
		output = -x;
	}
	else
	{
		output = x;
	}

	return output;
}

void copy_array(int16_t * in, int16_t * out, int64_t size)
{
	for(int64_t i = 0; i < size; i++)
	{
		out[i] = in[i];

	}
}

int16_t pitch_detection(int16_t* signal_in,int16_t Fs,int64_t size)
{
	int16_t L = SIZE_FFT;
	int16_t fft_out[SIZE_FFT];
	int16_t fft_out_i[SIZE_FFT];




		for(int16_t i = size; i < SIZE_FFT; i++){
			fft_out[i] = 0;
			fft_out_i[i] = 0;
		}

	copy_array(signal_in,fft_out,size);

	/*for(int64_t u = 0; u < size; u++){
			printf("%d ; ", fft_out[u]);
		}*/
	int scale = fix_fft(fft_out,fft_out_i, POWER_FFT, 0);

	int16_t fft_out_trunced[L];
	int16_t fft_out_i_trunced[L];
	int16_t fft_square[L];
	copy_array(fft_out,fft_out_trunced,L);
	copy_array(fft_out_i,fft_out_i_trunced,L);

	for(int16_t i = 0; i < L; i++)
	{
		int16_t fft_r_square = mul_int16_int16(fft_out_trunced[i],fft_out_trunced[i]);
		int16_t fft_i_square = mul_int16_int16(fft_out_i_trunced[i],fft_out_i_trunced[i]);
		fft_square[i] = fft_r_square + fft_i_square;
		//printf("%d %d -t ",i,fft_square[i]);
	}

	int16_t ifreq = indice_max(fft_square,L/2); // Q(16,15,0)
	//Fs/L = 7.8125 ~ 8 Q(16,15,0) revient a décaler de 3 à gauche
	//printf("freq : %d\n", ifreq << 3);
	int16_t freq = ifreq << 3; // Q(16,15,0)

	return freq;
}

int16_t tabC[] = {16,17,18,19,20,21,23,24,25,27,29,30,32,34,36,38,41,43,

        46,49,51,55,58,61,65,69,73,77,82,87,92,98,103,110,116,123,130,138,

        146,155,164,174,185,196,207,220,233,246,261,277,293,311,329,349,369,

        392,415,440,466,493,523,554,587,622,659,698,739,783,830,880,932,987,

        1046,1108,1174,1244,1318,1396,1479,1567,1661,1760,1864,1975,2093,2217,

        2349,2489,2637,2793,2959,3135,3322,3520,3729,3951};
int16_t new_pitch(int16_t fmax)
{
	int16_t table_size = 96;
	int16_t tab[table_size];
	for(int16_t i = 0; i < table_size; i++)
	{
		tab[i] = absolute(tabC[i]-fmax);
		//printf("%d  ",tab[i]);
	}
	int16_t i_tab = indice_min(tab, table_size);
	//printf("\ni_tab %d\n", i_tab);

		//printf("peach %d\n",tabC[i_tab]);
	return tabC[i_tab];

}

void pitch_synthesis(int16_t * sig_in,int16_t * sig_out, int16_t Fs,int64_t size)
{
	int16_t view_pow = 10;
	int16_t  l = 1<<view_pow; //Q(16,15,0);
	int16_t L = (int16_t)(size >>  view_pow); // taille de la view

	//printf("SIZE : %d\n",l);
	int16_t new_pitches[L];

	for(int16_t i = 0; i < L; i++)
	{
		int16_t fmax = pitch_detection(sig_in + i*l,Fs,l);
		new_pitches[i] = new_pitch(fmax);

		//sine_wave(sig_out,new_f,1<<8);
	}
	for(int16_t i = 0; i < L; i++)
	{

		sine_wave(sig_out + i*l,new_pitches[i],l);
	}
}

