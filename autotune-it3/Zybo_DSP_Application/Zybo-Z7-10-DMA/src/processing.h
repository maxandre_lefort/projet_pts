/*
 * processing.h
 *
 *  Created on: 11 janv. 2021
 *      Author: robin
 */

#ifndef SRC_PROCESSING_H_
#define SRC_PROCESSING_H_

#include <stdio.h>
#include <math.h>
#include "timers/timers.h"
#include "printers/printers.h"
#include "data_file.h"
#include <arm_acle.h> // DSP Intrinsics

/* Mathematical macros */
#define POWER_FFT 10
#define SIZE_FFT (1<<POWER_FFT)   // FIXME Assert SIZE_FFT = 2^POWER_FFT
#define M_PI   3.14159265358979323846


// Core methods 
int processing(int16_t* sig_out,int16_t* sig,int64_t size);
void benchmark(int16_t* sig_out,int16_t* sig,int64_t size);
int16_t fpsin(int16_t x);
int16_t mul_int16_int16(int16_t x,int16_t y);
void sine_wave(int16_t* sig_out, int16_t Fc, int64_t N);

// Additional user methods 
//TODO Iteration 3, 4: Add all stuff you need
int16_t pitch_detection(int16_t* signal_in,int16_t Fs,int64_t size);
int16_t absolute(int16_t x);
int16_t new_pitch(int16_t fmax);
void pitch_synthesis(int16_t * sig_in,int16_t * sig_out, int16_t Fs,int64_t size);
int16_t indice_max(int16_t * sig_in, int16_t size);
int16_t indice_min(int16_t * sig_in,int16_t size);
void copy_array(int16_t * in, int16_t * out, int64_t size);

//table






#endif /* SRC_PROCESSING_H_ */
