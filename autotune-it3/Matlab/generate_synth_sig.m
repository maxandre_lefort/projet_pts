
Fe = 8000;

sound_sig = f_generate_synth_sig(Fe);
% sound(sound_sig,Fe,16);

temps_frequence(sound_sig, Fe,0.1, 256, true)


function sig = discrete_sinus(F, Fe, Tmax)
    Te = 1/Fe;
    t_vector = 0:Te:Tmax;
    sig = sin(2*pi*F*t_vector);
end


function sig = f_generate_synth_sig(Fe)
   
   s1 =  discrete_sinus(30,Fe,0.06);
   s2 =  discrete_sinus(350,Fe,0.45-0.06);
   s3 =  discrete_sinus(2500,Fe,2.88-0.45);
   s4 =  discrete_sinus(1200,Fe,3.9-2.88);
   s5 =  discrete_sinus(400,Fe,5-3.9);
   
   sig = [s1,s2,s3,s4,s5];
end

function [time, freq, diagram] = temps_frequence(signal, Fe,Tv, fft_size, aff)
    %[sig,Fe] = audioread("../Base_Sound/"+fileName);
    [N,Z] = size(signal);
    N = max(N,Z);

    T_t = (N)/Fe;
    Te = 1/Fe;
    p = floor(Tv/Te);
    NbV = floor(N/p);
    pmin = fft_size;
    signal_trunced = signal(1:p*NbV);
    views = reshape(signal_trunced,p,NbV,[]);

    if p < pmin
        views_zero = zeros(pmin, NbV);
        views_zero(1:p,:) = views(:,:);
        views = views_zero;
       p = pmin;   
    end
    
    frequency_views = fft(views);
    frequency_views_folded = frequency_views(1:p/2,:);

    mod_spectre = 10*log10(abs(frequency_views_folded).^2);

    
    t_scale = 0:T_t;
    f_scale = 0:Fe/p:Fe/2;
    if aff == true
    figure;
    imagesc(t_scale,f_scale,mod_spectre)
    colorbar();
    set(gca,'YDir','normal');
    end

    time = t_scale;
    freq = f_scale;
    diagram = frequency_views_folded;
    
   
end
