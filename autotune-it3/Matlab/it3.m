% 3eme itération : Pitch detection and resynthesis
clc;
close all;
clear all;

cm_table = [16.35,17.32,18.35,19.45,20.60,21.83,23.12,24.50,25.96,27.50,29.14,30.87,32.70,34.65,36.71,38.89,41.20,43.65,46.25,49.00,51.91,55.00,58.27,61.74,65.41,69.30,73.42,77.78,82.41,87.31,92.50,98.00,103.83,110.00,116.54,123.47,130.81,138.59,146.83,155.56,164.81,174.61,185.00,196.00,207.65,220.00,233.08,246.94,261.63,277.18,293.66,311.13,329.63,349.23,369.99,392.00,415.30,440.00,466.16,493.88,523.25,554.37,587.33,622.25,659.25,698.46,739.99,783.99,830.61,880.00,932.33,987.77,1046.50,1108,73,1174.66,1244.51,1318.51,1396.91,1479.98,1567.98,1661.22,1760.00,1864.66,1975.53,2093.00,2217.46,2349.32,2489.02,2637.02,2793.83,2959.96,3135.96,3322.44,3520.00,3729.31,3951.07,4186.01,4434.92,4698.63,4978.03,5274.04,5587.65,5919.91,6271.93,6644.88,7040.00,7458.62,7902.13];

[sig, fs] = audioread("../Base_Sound/synth_sweep_1.wav");
temps_frequence_file("../Base_Sound/synth_sweep_1.wav",0.01,256);

synth_sig = pitch_synthesis(sig, fs, cm_table);
sound(synth_sig,fs);
figure;
plot(synth_sig);
temps_frequence(synth_sig,fs,0.01,256, true);

function fmax = pitch_detection(sig,fs)
    Fsig = fft(sig);
    aFsig = abs(Fsig);
    [max_f, i_max] = max(aFsig);
    fmax = (i_max-1)*fs/length(sig);
end 

function new_f = new_pitch(fmax, table)
    [diff_min,i_min] = min(abs(table-fmax));
    new_f = table(i_min);
end

function synth_sig = pitch_synthesis(sig, fs, table)
    L = 256;
    l = round(length(sig)/L);
    
    synth_sig = zeros(length(sig),1);
    
    for i = 1:1:L
        fmax = pitch_detection(sig(((i-1)*l)+1:i*l), fs);
        new_f = new_pitch(fmax, table);
        for k = ((i-1)*l)+1:1:i*l
            synth_sig(k) = sin(2*pi*new_f*k/fs);
        end
    end
end


function [time, freq, diagram] = temps_frequence(signal, Fe,Tv, fft_size, aff)
    %[sig,Fe] = audioread("../Base_Sound/"+fileName);
    [N,Z] = size(signal);

    T_t = (N)/Fe;
    Te = 1/Fe;
    p = floor(Tv/Te)
    NbV = floor(N/p)
    pmin = fft_size
    views = reshape(signal,p,NbV);
    if p < pmin
        views_zero = zeros(pmin, NbV);
        views_zero(1:p,:) = views(:,:);
        views = views_zero;
       p = pmin;   
    end
    
    frequency_views = fft(views);
    frequency_views_folded = frequency_views(1:p/2,:);

    mod_spectre = 10*log10(abs(frequency_views_folded).^2);

    
    t_scale = 0:T_t;
    f_scale = 0:Fe/p:Fe/2;
    if aff == true
    figure;
    imagesc(t_scale,f_scale,mod_spectre)
    colorbar();
    set(gca,'YDir','normal');
    end

    time = t_scale;
    freq = f_scale;
    diagram = frequency_views_folded;
    
   
end


function temps_frequence_file(fileName, Tv, fft_size)
        [sig,Fe] = audioread("../Base_Sound/"+fileName);
       temps_frequence(sig, Fe,Tv, fft_size,true)
       title("Time/frequency diagram of "+ fileName);
end


