close all;
clc;
% plotSound("synth_sweep_3.wav")
% temps_frequence_file("beep1.wav",0.0002,1024)
[sig,Fe] = audioread("../Base_Sound/synth_beep_1.wav");
signal_frequency(sig,Fe, 0.01, 512);


function plotSound(fileName) 
% --- loading the sound file
    [sig1,fs1] = audioread("../Base_Sound/"+fileName);
    [L,Z] = size(sig1);
    t = 0:1/fs1:(L-1)/fs1;
    figure;
    % --- plotting the sound in time domain
    plot(t,sig1)
    title(fileName + " in time domain");
    xlabel("time");
    ylabel("Amplitude");
    sigFreq1= fft(sig1);
    figure;
    % --- plotting the sound in frequency domain
    f = -fs1/2:fs1/L:(fs1)/2 ;
    size(f);
    size(abs(sigFreq1))
    sigFreq1 = [fftshift(sigFreq1);0];
    plot(f,abs(sigFreq1))
    title(fileName + " in frequency domain");
    xlabel("frequency");
    ylabel("Amplitude");


end


function [time, freq, diagram] = temps_frequence(signal, Fe,Tv, fft_size, aff)
    %[sig,Fe] = audioread("../Base_Sound/"+fileName);
    [N,Z] = size(signal);

    T_t = (N)/Fe;
    Te = 1/Fe;
    p = floor(Tv/Te);
    NbV = floor(N/p);
    pmin = fft_size;
    views = reshape(signal,p,NbV);
    if p < pmin
        views_zero = zeros(pmin, NbV);
        views_zero(1:p,:) = views(:,:);
        views = views_zero;
       p = pmin;   
    end
    
    frequency_views = fft(views);
    frequency_views_folded = frequency_views(1:p/2,:);

    mod_spectre = 10*log10(abs(frequency_views_folded).^2);

    
    t_scale = 0:T_t;
    f_scale = 0:Fe/p:Fe/2;
    if aff == true
    figure;
    imagesc(t_scale,f_scale,mod_spectre)
    colorbar();
    set(gca,'YDir','normal');
    end

    time = t_scale;
    freq = f_scale;
    diagram = frequency_views_folded;
    
   
end


function temps_frequence_file(fileName, Tv, fft_size)
        [sig,Fe] = audioread("../Base_Sound/"+fileName);
       temps_frequence(sig, Fe,Tv, fft_size,true)
       title("Time/frequency diagram of "+ fileName);
end

function m =  signal_frequency(signal,Fe, Tv, fft_size)
   [t,f,fft] = temps_frequence(signal, Fe,Tv, fft_size,false);

    [M,I] = max(fft);
    Im = round(mean(I));
    disp(Im)
    disp(f(Im))
    m = f(Im);
end
